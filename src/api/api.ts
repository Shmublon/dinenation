// @ts-ignore
import WooCommerceAPI from 'react-native-woocommerce-api';
import {IDish} from '../interfaces/IDish';
const WooCommerceAPIClient = new WooCommerceAPI({
  ssl: true,
  url: 'https://dinenation.com/',
  consumerKey: 'ck_11f370a58f0a180d08a5bfdc5fde205c200c3f21',
  consumerSecret: 'cs_198e26c66e91a1ac1fdc4d812173cf7dbefbd694',
  wpAPI: true, // Enable the WP REST API integration
  version: 'wc/v3', // WooCommerce WP REST API version
  queryStringAuth: true,
});

export const getProducts: () => Promise<IDish[]> = async () => {
  const data = await WooCommerceAPIClient.get('products', {
    category: 51,
  });
  const products: IDish[] = [];
  for (let i = 0; i < data.length; i++) {
    const dish: IDish = {
      id: 0,
      name: '',
      shortDescription: '',
      calories: 50,
      imageUrl: '',
      price: 0,
    };
    dish.name = data[i].name;
    dish.shortDescription = data[i].short_description.replace(
      new RegExp('<p>([^<]+)</p>[\\n]?'),
      '$1',
    );
    dish.calories = 75;
    dish.price = parseFloat(data[i].price);
    dish.id = data[i].id;
    if (data[i].images && data[i].images.length > 0) {
      dish.imageUrl = data[i].images[0].src;
    } else {
      dish.imageUrl =
        'https://res.cloudinary.com/hjzqny4m9/image/upload/v1614167317/dish_uuzkeu.png';
    }
    products.push(dish);
  }
  return products;
};
