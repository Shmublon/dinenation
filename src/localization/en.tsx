export default {
  somethingWentWrong: 'Something went wrong',
  auth: {
    signIn: 'Sign In',
    signOut: 'Sign Out',
  },
  navigation: {},
  button: 'Change language',
  mainSlogan: 'Your\nIndividual\nFood Plan',
  mainMenuTitle: 'Main menu',
};
