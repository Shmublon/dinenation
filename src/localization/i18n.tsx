import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {NativeModules, Platform} from 'react-native';
import en from './en';
import ru from './ru';

const resources = {
  en: {
    translation: en,
  },
  ru: {
    translation: ru,
  },
};

const locale =
  Platform.OS === 'ios'
    ? NativeModules.SettingsManager.settings.AppleLocale ||
      NativeModules.SettingsManager.settings.AppleLanguages[0] //iOS 13
    : NativeModules.I18nManager.localeIdentifier;

i18n.use(initReactI18next).init({
  resources,
  // lng: locale && locale.substring(0, 2) === 'ru' ? 'ru' : 'en',
  lng: 'ru',
  fallbackLng: 'en',
  debug: true,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
