import {ICaloriesData} from './ICaloriesData';
import {IDish} from './IDish';

export interface IOrder {
  dishesByDays: {
    monday: IDish[][];
    tuesday: IDish[][];
    wednesday: IDish[][];
    thursday: IDish[][];
    friday: IDish[][];
    saturday: IDish[][];
    sunday: IDish[][];
  };
  orderCalories: ICaloriesData;
  price: number;
  weeks: 1 | 4 | 13 | 26;
}
