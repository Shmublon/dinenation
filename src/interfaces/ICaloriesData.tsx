export interface ICaloriesData {
  calories: number;
  carbons: number;
  proteins: number;
  fats: number;
}
