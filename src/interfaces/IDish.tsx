export interface IDish {
  id: number;
  calories: number;
  name: string;
  shortDescription: string;
  imageUrl: string;
  price: number;
}
