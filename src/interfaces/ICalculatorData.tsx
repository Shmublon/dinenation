import {Gender} from '../enums/Genger';
import {Pregnancy} from '../enums/Pregnancy';
import {Intensity} from '../enums/Intensity';
import {DietType} from '../enums/DietType';
import {DietFeature} from '../enums/DietFeature';

export interface ICalculatorData {
  gender: Gender;
  pregnancy: Pregnancy;
  age: number;
  height: number;
  weight: number;
  workouts: number;
  target: number;
  intensity: Intensity;
  eatTimesPerDay: number;
  dietType: DietType;
  dietFeature: DietFeature;
}
