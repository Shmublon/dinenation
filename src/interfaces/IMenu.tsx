import {IDish} from './IDish';

export interface IMenu {
  dishesByDays: {
    monday: [IDish[], IDish[], IDish[]];
    tuesday: [IDish[], IDish[], IDish[]];
    wednesday: [IDish[], IDish[], IDish[]];
    thursday: [IDish[], IDish[], IDish[]];
    friday: [IDish[], IDish[], IDish[]];
    saturday: [IDish[], IDish[], IDish[]];
    sunday: [IDish[], IDish[], IDish[]];
  };
}
