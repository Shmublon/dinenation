import React, {useContext, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {context} from '../store/context';
import {LocalStorage} from '../utils/LocalStorage';
import {restoreToken, restoreUnregisteredData} from '../store/actions';
import LoadingScreen from '../screens/LoadingScreen';
import NotRegisteredUserNavigator from './NotRegisteredUserNavigator';
import RegisteredUserNavigator from './RegisteredUserNavigator';

const MainNavigator: React.FC = () => {
  const {dispatch, state} = useContext(context);

  useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    console.log('useEffect');
    const bootstrapAsync = async () => {
      let userToken;
      let calculatorData;
      let caloriesData;

      try {
        // await LocalStorage.removeItem('calculatorData');
        // await LocalStorage.removeItem('caloriesData');
        userToken = await LocalStorage.getItem('userToken');
        calculatorData = await LocalStorage.getItem('calculatorData');
        caloriesData = await LocalStorage.getItem('caloriesData');
      } catch (e) {
        // Restoring token failed
      }
      //TODO: validate token
      dispatch(restoreUnregisteredData(calculatorData, caloriesData));
      dispatch(restoreToken(userToken));
    };

    bootstrapAsync();
  }, [dispatch]);

  if (state.isLoading) {
    return <LoadingScreen />;
  }

  return (
    <NavigationContainer>
      {state.userToken == null ? (
        <NotRegisteredUserNavigator
          initialRouteName={
            state.calculatorData && state.caloriesData ? 'MainMenu' : 'Welcome'
          }
        />
      ) : (
        <RegisteredUserNavigator />
      )}
    </NavigationContainer>
  );
};

export default MainNavigator;
