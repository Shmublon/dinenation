import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import MainMenuScreen from '../screens/MainMenuScreen';
import ProfileScreen from '../screens/ProfileScreen';

const Stack = createStackNavigator();

const RegisteredUserNavigator: React.FC = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen name="MainMenu" component={MainMenuScreen} />
    </Stack.Navigator>
  );
};

export default RegisteredUserNavigator;
