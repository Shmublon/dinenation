import {
  createStackNavigator,
  StackNavigationProp,
  TransitionPresets,
} from '@react-navigation/stack';
import React from 'react';
import WelcomeScreen from '../screens/WelcomeScreen';
import SignInScreen from '../screens/SignInScreen';
import MainMenuScreen from '../screens/MainMenuScreen';
import CalculatorScreen from '../screens/CalculatorScreen';
import {useTranslation} from 'react-i18next';

const Stack = createStackNavigator();

interface Props {
  initialRouteName: string;
}

const NotRegisteredUserNavigator: React.FC<Props> = ({initialRouteName}) => {
  const {t} = useTranslation();
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={initialRouteName}>
      <Stack.Screen
        options={{
          ...TransitionPresets.ModalSlideFromBottomIOS,
        }}
        name="Welcome"
        component={WelcomeScreen}
      />
      <Stack.Screen name="SignIn" component={SignInScreen} />
      <Stack.Screen
        options={{headerTitle: t('personalInfo'), gestureEnabled: false}}
        name="Calculator"
        component={CalculatorScreen}
      />
      <Stack.Screen
        options={{
          gestureEnabled: false,
          ...TransitionPresets.DefaultTransition,
        }}
        name="MainMenu"
        component={MainMenuScreen}
      />
    </Stack.Navigator>
  );
};

export default NotRegisteredUserNavigator;
