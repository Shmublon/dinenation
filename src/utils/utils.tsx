import {Duration} from '../enums/Duration';

export const getDurationText = (duration: Duration) => {
  switch (duration) {
    case Duration.WEEK:
      return 'week';
    case Duration.MONTH:
      return 'month';
    case Duration.THREE_MONTH:
      return 'threeMonths';
    case Duration.SIX_MONTH:
      return 'sixMonths';
  }
}
