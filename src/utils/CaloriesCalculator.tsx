import {Gender} from '../enums/Genger';
import {Target} from '../enums/Target';
import {Intensity} from '../enums/Intensity';
import {ICalculatorData} from '../interfaces/ICalculatorData';

export const CaloriesCalculator = {
  calculateCalories: (userData: ICalculatorData | null) => {
    if (!userData) {
      return null;
    }
    const {gender, weight, height, age, workouts, intensity, target} = userData;
    let calories = 10 * weight + 6.25 * height - 5 * age + 5;
    let proteins = weight * 1.4;
    let fats = 0;
    let carbons;
    if (gender === Gender.FEMALE) {
      calories = calories - 166;
      fats = weight;
      if (weight < 51) {
        fats = weight * 1.2;
      }
      if (Target.LOSE) {
        fats = fats - 6;
      }
    }

    if (gender === Gender.MALE) {
      fats = weight * 0.8;
      if (Target.LOSE) {
        fats = (weight - 5) * 0.8;
      }
    }

    switch (workouts) {
      case 0: {
        calories = calories * 1.2;
        break;
      }
      case 1:
      case 2:
      case 3: {
        calories = calories * 1.375;
        proteins = weight * 1.6;
        break;
      }
      case 4:
      case 5: {
        calories = calories * 1.465;
        proteins = weight * 2;
        break;
      }
      case 6:
      case 7: {
        calories = calories * 1.725;
        proteins = weight * 2.5;
        break;
      }
    }

    if (target === Target.LOSE) {
      switch (intensity) {
        case Intensity.SLOW: {
          calories = calories * 0.9;
          break;
        }
        case Intensity.NORMAL: {
          calories = calories * 0.8;
          break;
        }
        case Intensity.INTENSELY: {
          calories = calories * 0.6;
          break;
        }
      }
    }

    if (target === Target.GAIN) {
      switch (intensity) {
        case Intensity.SLOW: {
          calories = calories * 1.1;
          break;
        }
        case Intensity.NORMAL: {
          calories = calories * 1.2;
          break;
        }
        case Intensity.INTENSELY: {
          calories = calories * 1.4;
          break;
        }
      }
    }

    carbons = calories - (fats * 9 + proteins * 4);

    if (carbons < 0) {
      carbons = 0;
    }

    return {
      calories: Math.round(calories),
      carbons: Math.round(carbons / 4),
      proteins: Math.round(proteins),
      fats: Math.round(fats),
    };
    // В одном грамме углеводов и белков — 4 ккал, жиров — 9 ккал.
  },
};
