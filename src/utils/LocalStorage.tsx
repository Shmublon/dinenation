import AsyncStorage from '@react-native-async-storage/async-storage';

export const LocalStorage = {
  setItem: (key: string, value: Object | null) => {
    if (!value) {
      return null;
    }
    return AsyncStorage.setItem(key, JSON.stringify(value));
  },

  getItem: async (key: string) => {
    const data = await AsyncStorage.getItem(key);
    return data ? JSON.parse(data) : undefined;
  },

  removeItem: (key: string) => {
    return AsyncStorage.removeItem(key);
  },
};
