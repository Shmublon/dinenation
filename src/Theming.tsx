import {createTheming} from '@callstack/react-theme-provider';
import type {ThemingType} from '@callstack/react-theme-provider';

export type Theme = {
  primaryColor: string;
  accentColor: string;
  backgroundColor: string;
  textColor: string;
  secondaryColor: string;
  inactiveColor: string;
  inactive2: string;
  lightGreenColor: string;
  yellowColor: string;
};

export const themes: {[key: string]: Theme} = {
  default: {
    primaryColor: '#FFFFFF',
    accentColor: '#FF4713',
    backgroundColor: '#EFF3F4',
    textColor: '#0C0C0C',
    secondaryColor: '#0C783C',
    lightGreenColor: '#40C677',
    inactiveColor: '#68696D',
    inactive2: '#C0C0C0',
    yellowColor: '#F1AC1D',
  },
  dark: {
    primaryColor: '#FFFFFF',
    accentColor: '#FF4713',
    backgroundColor: '#EFF3F4',
    textColor: '#0C0C0C',
    secondaryColor: '#0C783C',
    lightGreenColor: '#40C677',
    inactiveColor: '#68696D',
    inactive2: '#C0C0C0',
    yellowColor: '#F1AC1D',
  },
};

const {ThemeProvider, withTheme}: ThemingType<Theme> = createTheming(
  themes.default,
);

export {ThemeProvider, withTheme};
