import {ICalculatorData} from '../interfaces/ICalculatorData';
import {ICaloriesData} from '../interfaces/ICaloriesData';

export const RESTORE_TOKEN = 'RESTORE_TOKEN';
export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';
export const SAVE_CALCULATOR_DATA = 'SAVE_CALCULATOR_DATA';
export const SAVE_CALORIES_DATA = 'SAVE_CALORIES_DATA';
export const RESTORE_UNREGISTERED_DATA = 'RESTORE_UNREGISTERED_DATA';

export const restoreToken = (userToken: string) => ({
  type: RESTORE_TOKEN,
  token: userToken,
});

export const signIn = (userToken: string) => ({
  type: SIGN_IN,
  token: userToken,
});

export const signOut = () => ({
  type: SIGN_OUT,
});

export const saveCalculatorData = (calculatorData: ICalculatorData) => ({
  type: SAVE_CALCULATOR_DATA,
  calculatorData,
});

export const saveCaloriesData = (caloriesData: ICaloriesData) => ({
  type: SAVE_CALORIES_DATA,
  caloriesData,
});

export const restoreUnregisteredData = (
  calculatorData: ICalculatorData,
  caloriesData: ICaloriesData,
) => ({
  type: RESTORE_UNREGISTERED_DATA,
  caloriesData,
  calculatorData,
});
