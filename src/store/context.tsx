import React, {createContext, useReducer} from 'react';
import {
  RESTORE_TOKEN,
  RESTORE_UNREGISTERED_DATA,
  SAVE_CALCULATOR_DATA,
  SAVE_CALORIES_DATA,
  SIGN_IN,
  SIGN_OUT,
} from './actions';
import {ICalculatorData} from '../interfaces/ICalculatorData';
import {ICaloriesData} from '../interfaces/ICaloriesData';

type Action =
  | {type: 'RESTORE_TOKEN'; token: string | null}
  | {type: 'SIGN_IN'; token: string | null}
  | {type: 'SIGN_OUT'}
  | {
      type: 'SAVE_CALCULATOR_DATA';
      calculatorData: ICalculatorData | null;
      token: string | null;
    }
  | {
      type: 'SAVE_CALORIES_DATA';
      caloriesData: ICaloriesData | null;
      token: string | null;
    }
  | {
      type: 'RESTORE_UNREGISTERED_DATA';
      caloriesData: ICaloriesData | null;
      calculatorData: ICalculatorData | null;
      token: string | null;
    };

type State = {
  isLoading: boolean;
  isSignout: boolean;
  userToken: string | null;
  calculatorData: ICalculatorData | null;
  caloriesData: ICaloriesData | null;
};

const initialState = {
  dispatch: (test: any) => {
    test.toString();
  },
  state: {
    isLoading: true,
    isSignout: false,
    userToken: null,
    calculatorData: null,
    caloriesData: null,
  },
};
const context = createContext(initialState);
const {Provider} = context;

const StateProvider: React.FC = ({children}) => {
  const [state, dispatch] = useReducer(
    (prevState: State, action: Action) => {
      switch (action.type) {
        case RESTORE_TOKEN:
          return {
            ...prevState,
            userToken: action.token,
          };
        case SIGN_IN:
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case SIGN_OUT:
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
        case SAVE_CALCULATOR_DATA:
          return {
            ...prevState,
            calculatorData: action.calculatorData,
          };
        case SAVE_CALORIES_DATA:
          return {
            ...prevState,
            caloriesData: action.caloriesData,
          };
        case RESTORE_UNREGISTERED_DATA:
          return {
            ...prevState,
            caloriesData: action.caloriesData,
            calculatorData: action.calculatorData,
            isLoading: false,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
      calculatorData: null,
      caloriesData: null,
    },
  );

  // @ts-ignore
  return <Provider value={{state, dispatch}}>{children}</Provider>;
};

export {context, StateProvider};
