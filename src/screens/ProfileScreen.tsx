import React, {useContext} from 'react';
import {StatusBar} from 'react-native';
import {useTranslation} from 'react-i18next';
import SafeContainer from '../components/basics/SafeContainer';
import MainButton from '../components/buttons/MainButton';
import {context} from '../store/context';
import {signOut} from '../store/actions';
import BoldText from '../components/text/BoldText';

const ProfileScreen: React.FC = () => {
  const {t} = useTranslation();
  const {dispatch} = useContext(context);

  return (
    <SafeContainer>
      <BoldText fontSize={30} style={{marginTop: '30%'}} text={t('profile')} />
      <MainButton
        text={t('auth.signOut')}
        onPress={() => dispatch(signOut())}
      />
    </SafeContainer>
  );
};

export default ProfileScreen;
