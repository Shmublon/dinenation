import React, {useContext, useEffect, useRef, useState} from 'react';
import {useTranslation} from 'react-i18next';
import SafeContainer from '../components/basics/SafeContainer';
import {StackNavigationProp} from '@react-navigation/stack';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import Animated, {diffClamp, interpolate} from 'react-native-reanimated';
import {context} from '../store/context';
import Row from '../components/basics/Row';
import BoldText from '../components/text/BoldText';
import {Theme, withTheme} from '../Theming';
import SubMenuButton from '../components/mainMenu/SubMenuButton';
import DurationIcon from '../components/icons/DurationIcon';
import DeliveryIcon from '../components/icons/DeliveryIcon';
import WeekDaysTabs from '../components/tabs/WeekDaysTabs';
import MenuFooter from '../components/mainMenu/MenuFooter';
import MenuHeader from '../components/mainMenu/MenuHeader';
import {getProducts} from '../api/api';
import {IMenu} from '../interfaces/IMenu';
import {IDish} from '../interfaces/IDish';
import DishSlider from '../components/mainMenu/DishSlider';
import VerticalSpace from '../components/basics/VerticalSpace';
import DurationDetails from '../components/mainMenu/DurationDetails';
import BottomSheet, {BottomSheetBackdrop} from '@gorhom/bottom-sheet';
import BottomSheetHandleComponent from '../components/bottomSheet/BottomSheetHandleComponent';
import {Duration} from '../enums/Duration';
import {getDurationText} from '../utils/utils';
import {saveCalculatorData, saveCaloriesData} from '../store/actions';
import {LocalStorage} from '../utils/LocalStorage';
import MealDetails from '../components/mainMenu/MealDetails';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

interface Props {
  navigation: StackNavigationProp<any>;
  theme: Theme;
}

const ANIMATED_HEADER_HEIGHT = 126;
const WEEK_DAYS_CONTAINER_HEIGHT = 90;

const MainMenuScreen: React.FC<Props> = ({navigation, theme}) => {
  const {t} = useTranslation();
  const scrollY = useRef(new Animated.Value(0)).current;
  const clamp = diffClamp(scrollY, 0, ANIMATED_HEADER_HEIGHT);
  const {dispatch, state} = useContext(context);
  const [weekDay, setWeekDay] = useState(1);
  const [isMenuLoading, setMenuLoading] = useState(true);
  const [menu, setMenu] = useState<IMenu>();
  // @ts-ignore
  const {calories, proteins, fats, carbons} = state.caloriesData;
  // @ts-ignore
  const {eatTimesPerDay} = state.calculatorData;
  const [firstMeal, setFirstMeal] = useState<IDish[]>([]);
  const [secondMeal, setSecondMeal] = useState<IDish[]>([]);
  const [thirdMeal, setThirdMeal] = useState<IDish[]>([]);
  const [duration, setDuration] = useState<Duration>(Duration.WEEK);
  const durationSheetRef = React.useRef<BottomSheet>(null);
  const mealSheetRef = React.useRef<BottomSheet>(null);
  const insets = useSafeAreaInsets();

  useEffect(() => {
    const getData = async () => {
      // const dishes = await getProducts();
      // const mainDish = dishes[0];
      const mainDish = {
        calories: 75,
        id: 5565,
        imageUrl:
          'https://res.cloudinary.com/hjzqny4m9/image/upload/v1614167317/dish_uuzkeu.png',
        name: 'Salmon',
        price: 6,
        shortDescription: 'Salmon baked with veg',
      };

      const createdMenu: IMenu = {
        dishesByDays: {
          monday: [[], [], []],
          tuesday: [[], [], []],
          wednesday: [[], [], []],
          thursday: [[], [], []],
          friday: [[], [], []],
          saturday: [[], [], []],
          sunday: [[], [], []],
        },
      };
      const dayMenu: [IDish[], IDish[], IDish[]] = [[], [], []];
      for (let i = 0; i < dayMenu.length; i++) {
        for (let j = 0; j < 10; j++) {
          const randomNumber = Math.round(Math.random() * 1000000);
          const newDish = Object.assign({}, mainDish);
          newDish.id = randomNumber;
          dayMenu[i].push(newDish);
        }
      }
      createdMenu.dishesByDays.monday = dayMenu;
      createdMenu.dishesByDays.tuesday = dayMenu;
      createdMenu.dishesByDays.wednesday = dayMenu;
      createdMenu.dishesByDays.thursday = dayMenu;
      createdMenu.dishesByDays.friday = dayMenu;
      createdMenu.dishesByDays.saturday = dayMenu;
      createdMenu.dishesByDays.sunday = dayMenu;
      setMenu(createdMenu);
    };
    setMenuLoading(false);
    getData();
  }, []);

  const renderMenuSliders = () => {
    if (!isMenuLoading) {
      return (
        <Animated.ScrollView
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: scrollY}}}],
            {useNativeDriver: true},
          )}
          bounces={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.menuContentContainerStyle}>
          <View
            style={[
              styles.menuContainer,
              {backgroundColor: theme.backgroundColor},
            ]}>
            <VerticalSpace space={100} />
            <DishSlider
              setAddedDishes={setFirstMeal}
              sliderLabel={t('firstMeal')}
              dishes={menu?.dishesByDays.monday[0]}
              addedDishes={firstMeal}
              onDishPress={(dish) =>
                console.log('navigate to dish, index: ', dish)
              }
            />
            <VerticalSpace space={16} />
            <DishSlider
              sliderLabel={t('secondMeal')}
              dishes={menu?.dishesByDays.monday[1]}
              addedDishes={secondMeal}
              setAddedDishes={setSecondMeal}
              onDishPress={(dish) =>
                console.log('navigate to dish, index: ', dish)
              }
            />
            <VerticalSpace space={16} />
            <DishSlider
              sliderLabel={t('thirdMeal')}
              dishes={menu?.dishesByDays.monday[2]}
              addedDishes={thirdMeal}
              setAddedDishes={setThirdMeal}
              onDishPress={(dish) =>
                console.log('navigate to dish, index: ', dish)
              }
            />
          </View>
        </Animated.ScrollView>
      );
    }
  };

  const translateY = interpolate(clamp, {
    inputRange: [0, ANIMATED_HEADER_HEIGHT],
    outputRange: [0, -ANIMATED_HEADER_HEIGHT],
  });

  const opacity = interpolate(clamp, {
    inputRange: [0, ANIMATED_HEADER_HEIGHT],
    outputRange: [1, 0],
  });

  return (
    <>
      <SafeContainer
        safeAreaColor={theme.primaryColor}
        style={{backgroundColor: theme.primaryColor}}>
        {renderMenuSliders()}
        <Animated.View
          style={[styles.headerContainer, {transform: [{translateY}]}]}>
          <View style={{backgroundColor: theme.primaryColor}}>
            <Animated.View style={{opacity}}>
              <Row style={styles.header}>
                <BoldText
                  style={styles.headerText}
                  text={t('menuHeaderText')}
                  fontSize={18}
                />
                <TouchableOpacity
                  onPress={async () => {
                    await LocalStorage.removeItem('calculatorData');
                    await LocalStorage.removeItem('caloriesData');
                    navigation.navigate('Welcome');
                  }}
                  style={styles.profileButton}>
                  <Image
                    source={require('../../assets/images/profile_stub.png')}
                    style={styles.profileImage}
                  />
                </TouchableOpacity>
              </Row>
              <Row>
                <SubMenuButton
                  onPress={() => durationSheetRef.current?.expand()}
                  mainText={t(getDurationText(duration))}
                  secondaryText={'Длительность'}
                  icon={() => (
                    <DurationIcon size={100} color={theme.lightGreenColor} />
                  )}
                />
                <SubMenuButton
                  mainText={'15 мар, пн'}
                  secondaryText={'Первая доставка'}
                  icon={() => (
                    <DeliveryIcon size={100} color={theme.lightGreenColor} />
                  )}
                />
              </Row>
              <View
                style={[styles.separator, {backgroundColor: theme.textColor}]}
              />
            </Animated.View>
          </View>
          <WeekDaysTabs
            style={[
              styles.weekDaysContainer,
              {backgroundColor: theme.primaryColor},
            ]}
            selectedIndex={weekDay}
            onItemPress={setWeekDay}
            values={['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС']}
          />
          <VerticalSpace space={16} />
          <MenuHeader
            calories={calories}
            proteins={proteins}
            fats={fats}
            carbons={carbons}
            eatTimesPerDay={eatTimesPerDay + 1}
            onFilterPress={() => console.log('on header filter press')}
            onInfoPress={() => mealSheetRef.current?.expand()}
            filterCounter={5}
          />
        </Animated.View>
        <MenuFooter onOrderPress={() => console.log('order')} />
      </SafeContainer>
      <BottomSheet
        backdropComponent={BottomSheetBackdrop}
        ref={durationSheetRef}
        snapPoints={[0, 550 + insets.bottom]}
        handleComponent={() => (
          <BottomSheetHandleComponent
            style={{backgroundColor: theme.backgroundColor}}
          />
        )}>
        <DurationDetails
          selectedValue={duration}
          onTabPress={(selectedDuration) => {
            setDuration(selectedDuration);
            durationSheetRef.current?.close();
          }}
        />
      </BottomSheet>
      <BottomSheet
        backdropComponent={BottomSheetBackdrop}
        ref={mealSheetRef}
        snapPoints={[0, 284 + insets.bottom]}
        handleComponent={() => (
          <BottomSheetHandleComponent
            style={{backgroundColor: theme.backgroundColor}}
          />
        )}>
        <MealDetails />
      </BottomSheet>
    </>
  );
};

export default withTheme(MainMenuScreen);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },
  menuContainer: {
    borderRadius: 25,
    flex: 1,
    paddingBottom: 200,
  },
  header: {
    height: 55,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerText: {
    paddingLeft: 16,
  },
  profileButton: {
    height: '100%',
    width: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileImage: {
    height: 38,
    width: 38,
    borderRadius: 10,
  },
  separator: {
    width: '100%',
    height: 1,
    opacity: 0.1,
  },
  weekDaysContainer: {
    height: WEEK_DAYS_CONTAINER_HEIGHT,
  },
  headerContainer: {
    position: 'absolute',
    width: '100%',
  },
  menuContentContainerStyle: {
    paddingTop: ANIMATED_HEADER_HEIGHT + WEEK_DAYS_CONTAINER_HEIGHT,
  },
});
