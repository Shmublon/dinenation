import React from 'react';
import {ActivityIndicator} from 'react-native';
import {Theme, withTheme} from '../Theming';
import SafeContainer from '../components/basics/SafeContainer';

interface Props {
  theme: Theme;
}

const LoadingScreen: React.FC<Props> = ({theme}) => {
  return (
    <SafeContainer>
      <ActivityIndicator
        size={'large'}
        color={theme.accentColor}
        style={{marginTop: 200}}
      />
    </SafeContainer>
  );
};

export default withTheme(LoadingScreen);
