import React, {useContext} from 'react';
import {useTranslation} from 'react-i18next';
import SafeContainer from '../components/basics/SafeContainer';
import MainButton from '../components/buttons/MainButton';
import {context} from '../store/context';
import {signIn} from '../store/actions';
import BoldText from '../components/text/BoldText';

const SignInScreen: React.FC = () => {
  const {t} = useTranslation();
  const {dispatch, state} = useContext(context);

  return (
    <SafeContainer style={{justifyContent: 'space-between'}}>
      <BoldText fontSize={30} style={{marginTop: '30%'}} text={'SIGN IN'} />
      <MainButton
        text={t('auth.signIn')}
        onPress={() => dispatch(signIn('test-token'))}
      />
    </SafeContainer>
  );
};

export default SignInScreen;
