import React from 'react';
import {StyleSheet, Text, useWindowDimensions, View} from 'react-native';
import {useTranslation} from 'react-i18next';
import SafeContainer from '../components/basics/SafeContainer';
import MainButton from '../components/buttons/MainButton';
import {StackNavigationProp} from '@react-navigation/stack';
// @ts-ignore
import Video from 'react-native-video';
import Logo from '../components/Logo';
import Center from '../components/basics/Center';
import {Theme, withTheme} from '../Theming';
import OutLinedButton from '../components/buttons/OutLinedButton';
import VerticalSpace from '../components/basics/VerticalSpace';
import NormalText from '../components/text/NormalText';
import TextBullet from '../components/text/TextBullet';
import BoldText from '../components/text/BoldText';
import {Fonts} from '../constants/Fonts';

interface Props {
  navigation: StackNavigationProp<any>;
  theme: Theme;
}

const video = require('../../assets/video.mov');

const WelcomeScreen: React.FC<Props> = ({navigation, theme}) => {
  const {t} = useTranslation();
  const windowWidth = useWindowDimensions().width;
  const windowHeight = useWindowDimensions().height;
  const backVideoDimensions = {
    height: windowHeight,
    width: windowWidth,
  };

  const renderBullets = () => {
    return (
      <View>
        <TextBullet text={t('welcomeBullet1')} />
        <VerticalSpace space={16} />
        <TextBullet text={t('welcomeBullet2')} />
        <VerticalSpace space={16} />
        <TextBullet text={t('welcomeBullet3')} />
      </View>
    );
  };

  return (
    <>
      <Video
        source={video}
        style={[styles.backgroundVideo, backVideoDimensions]}
        resizeMode={'cover'}
        muted
        repeat
      />
      <View
        style={[
          styles.onVideo,
          backVideoDimensions,
          {backgroundColor: theme.primaryColor},
        ]}
      />
      <SafeContainer style={styles.container}>
        <Center style={{marginTop: '10%'}}>
          <Logo size={100} />
        </Center>
        <View>
          <BoldText fontSize={30} centerText text={t('mainSlogan')} />
          <VerticalSpace space={16} />
          <NormalText
            style={styles.welcomeText}
            fontSize={14}
            centerText
            text={t('welcomeText')}
          />
        </View>
        {renderBullets()}
        <View>
          <Text
            onPress={() => navigation.push('MainMenu')}
            style={[
              styles.text,
              {
                color: theme.textColor,
              },
            ]}>
            {t('welcomeText2')}{' '}
            <Text style={styles.textMySelf}>{t('myself')}</Text>
          </Text>
          <VerticalSpace space={24} />
          <MainButton
            text={t('calculateMenu')}
            onPress={() => navigation.push('Calculator')}
          />
          <VerticalSpace space={16} />
          <OutLinedButton
            text={t('signIn')}
            onPress={() => navigation.push('SignIn')}
          />
          <VerticalSpace space={24} />
        </View>
      </SafeContainer>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    justifyContent: 'space-between',
  },

  welcomeText: {
    paddingHorizontal: 16,
  },

  backgroundVideo: {
    position: 'absolute',
  },

  onVideo: {
    position: 'absolute',
    opacity: 0.5,
  },

  text: {
    fontFamily: Fonts.regular,
    fontWeight: '400',
    fontSize: 14,
    textAlign: 'center',
    paddingHorizontal: 16,
  },

  textMySelf: {
    textDecorationLine: 'underline',
  },
});

export default withTheme(WelcomeScreen);
