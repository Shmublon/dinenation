import React, {useContext, useRef, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import SafeContainer from '../components/basics/SafeContainer';
import ViewPager from '@react-native-community/viewpager';
import {StackNavigationProp} from '@react-navigation/stack';
import CalculatorPersonalInfoStep from '../components/calculator/CalculatorPersonalInfoStep';
import CalculatorTargetStep from '../components/calculator/CalculatorTargetStep';
import CalculatorDietStep from '../components/calculator/CalculatorDietStep';
import {context} from '../store/context';

interface Props {
  navigation: StackNavigationProp<any>;
}

const CalculatorScreen: React.FC<Props> = ({navigation}) => {
  const {dispatch, state} = useContext(context);
  const pager = useRef<ViewPager>({} as ViewPager);
  const [page, setPage] = useState(0);

  const onBack = () =>
    page > 0 ? pager.current?.setPage(page - 1) : navigation.goBack();

  const onNext = () =>
    page < 2
      ? pager.current?.setPage(page + 1)
      : navigation.navigate('MainMenu');

  return (
    <SafeContainer>
      <ViewPager
        ref={pager}
        scrollEnabled={false}
        style={styles.viewPager}
        onPageSelected={(e) => {
          setPage(e.nativeEvent.position);
        }}
        initialPage={0}>
        <View key={'1'}>
          <CalculatorPersonalInfoStep onNext={onNext} onBack={onBack} />
        </View>
        <View key={'2'}>
          <CalculatorTargetStep onNext={onNext} onBack={onBack} />
        </View>
        <View key={'3'}>
          <CalculatorDietStep onNext={onNext} onBack={onBack} />
        </View>
      </ViewPager>
    </SafeContainer>
  );
};

export default CalculatorScreen;

const styles = StyleSheet.create({
  viewPager: {
    flex: 1,
  },
});
