import React, {useContext, useState} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {useTranslation} from 'react-i18next';
import VerticalSpace from '../basics/VerticalSpace';
import BoldText from '../text/BoldText';
import WhiteCard from '../cards/WhiteCard';
import HorizontalTabs from '../tabs/HorizontalTabs';
import VerticalTabs from '../tabs/VerticalTabs';
import MeasuresSlider from '../slider/MeasuresSlider';
import Center from '../basics/Center';
import CalculatorFooter from './CalculatorFooter';
import {context} from '../../store/context';
import {saveCalculatorData} from '../../store/actions';
import {Gender} from '../../enums/Genger';
import {Pregnancy} from '../../enums/Pregnancy';

const initialUserHeight = 170;
const initialAge = 30;
const initialWeight = 60;

interface Props {
  onBack: () => void;
  onNext: () => void;
}

const CalculatorPersonalInfoStep: React.FC<Props> = ({onNext, onBack}) => {
  const {dispatch, state} = useContext(context);
  const {t} = useTranslation();
  const [gender, setGender] = useState(Gender.FEMALE);
  const [pregnancy, setPregnancy] = useState(Pregnancy.NOT_PREGNANT);
  const [age, setAge] = useState(initialAge);
  const [userHeight, setUserHeight] = useState(initialUserHeight);
  const [weight, setWeight] = useState(initialWeight);

  return (
    <>
      <ScrollView
        style={styles.container}
        contentContainerStyle={{paddingBottom: 40}}
        showsVerticalScrollIndicator={false}>
        <Center style={styles.header}>
          <BoldText fontSize={18} text={t('personalInfo')} />
        </Center>
        <VerticalSpace space={6} />
        <BoldText fontSize={14} centerText text={t('gender')} />
        <VerticalSpace space={12} />
        <WhiteCard padding={8}>
          <HorizontalTabs
            selectedIndex={gender}
            onChange={setGender}
            values={[t('genderFemale'), t('genderMale')]}
          />
        </WhiteCard>
        {gender === 0 && (
          <>
            <VerticalSpace space={14} />
            <WhiteCard padding={8}>
              <VerticalTabs
                onTabChange={setPregnancy}
                values={[t('notPregnant'), t('pregnant'), t('nursing')]}
              />
            </WhiteCard>
          </>
        )}
        <VerticalSpace space={16} />
        <WhiteCard padding={16}>
          <BoldText fontSize={14} centerText text={t('age')} />
          <VerticalSpace space={24} />
          <MeasuresSlider
            onActiveIndexChange={setAge}
            initialValue={initialAge}
            start={10}
            end={110}
          />
        </WhiteCard>
        <VerticalSpace space={16} />
        <WhiteCard padding={16}>
          <BoldText fontSize={14} centerText text={t('height')} />
          <VerticalSpace space={24} />
          <MeasuresSlider
            onActiveIndexChange={setUserHeight}
            initialValue={initialUserHeight}
            start={100}
            end={250}
          />
        </WhiteCard>
        <VerticalSpace space={16} />
        <WhiteCard padding={16}>
          <BoldText fontSize={14} centerText text={t('weight')} />
          <VerticalSpace space={24} />
          <MeasuresSlider
            onActiveIndexChange={setWeight}
            initialValue={initialWeight}
            start={30}
            end={200}
          />
        </WhiteCard>
      </ScrollView>
      <CalculatorFooter
        onBack={onBack}
        onNext={() => {
          dispatch(
            saveCalculatorData({
              // @ts-ignore
              ...state.calculatorData,
              age,
              weight,
              height: userHeight,
              pregnancy,
              gender,
            }),
          );
          onNext();
        }}
      />
    </>
  );
};

export default CalculatorPersonalInfoStep;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },

  header: {
    height: 55,
  },
});
