import React, {useContext, useState} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {useTranslation} from 'react-i18next';
import Center from '../basics/Center';
import BoldText from '../text/BoldText';
import VerticalSpace from '../basics/VerticalSpace';
import WhiteCard from '../cards/WhiteCard';
import VerticalTabs from '../tabs/VerticalTabs';
import HorizontalTabs from '../tabs/HorizontalTabs';
import CalculatorFooter from './CalculatorFooter';
import {context} from '../../store/context';
import {saveCalculatorData, saveCaloriesData} from '../../store/actions';
import {DietType} from '../../enums/DietType';
import {DietFeature} from '../../enums/DietFeature';
import {CaloriesCalculator} from '../../utils/CaloriesCalculator';
import {LocalStorage} from '../../utils/LocalStorage';
import {ICaloriesData} from '../../interfaces/ICaloriesData';

interface Props {
  onBack: () => void;
  onNext: () => void;
}

const initialEatTimesPerDay = 3;

const CalculatorDietStep: React.FC<Props> = ({onNext, onBack}) => {
  const {t} = useTranslation();
  const {dispatch, state} = useContext(context);
  const [eatTimesPerDay, setEatTimesPerDay] = useState(
    initialEatTimesPerDay - 1,
  );
  const [dietType, setDietType] = useState(DietType.NO_LIMITS);
  const [dietFeature, setDietFeature] = useState(DietFeature.NO_FEATURES);

  return (
    <>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainerStyle}
        showsVerticalScrollIndicator={false}>
        <Center style={styles.header}>
          <BoldText fontSize={18} text={t('chooseYourDiet')} />
        </Center>
        <WhiteCard padding={8}>
          <VerticalTabs
            onTabChange={setDietType}
            values={[
              t('noLimit'),
              t('noMeat'),
              t('vegetarian'),
              t('vegan'),
              t('noCarbons'),
            ]}
          />
        </WhiteCard>
        <VerticalSpace space={16} />
        <WhiteCard padding={8}>
          <VerticalSpace space={8} />
          <BoldText centerText fontSize={14} text={t('dietFeatures')} />
          <VerticalSpace space={20} />
          <VerticalTabs
            onTabChange={setDietFeature}
            values={[t('noFeatures'), t('glutenFree'), t('lactoseFree')]}
          />
        </WhiteCard>
        <VerticalSpace space={16} />
        <WhiteCard padding={8}>
          <VerticalSpace space={8} />
          <BoldText centerText fontSize={14} text={t('eatTimesPerDay')} />
          <VerticalSpace space={20} />
          <HorizontalTabs
            onChange={(index) => setEatTimesPerDay(index + 1)}
            selectedIndex={eatTimesPerDay}
            values={['1', '2', '3']}
          />
        </WhiteCard>
      </ScrollView>
      <CalculatorFooter
        onBack={onBack}
        onNext={async () => {
          const calculatorData = {
            // @ts-ignore
            ...state.calculatorData,
            eatTimesPerDay,
            dietType,
            dietFeature,
          };
          // @ts-ignore
          const caloriesData: ICaloriesData = CaloriesCalculator.calculateCalories(
            calculatorData,
          );
          dispatch(saveCaloriesData(caloriesData));
          await LocalStorage.setItem('calculatorData', calculatorData);
          await LocalStorage.setItem('caloriesData', caloriesData);
          dispatch(saveCalculatorData(calculatorData));
          onNext();
        }}
      />
    </>
  );
};

export default CalculatorDietStep;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },

  header: {
    height: 55,
  },
  contentContainerStyle: {
    paddingBottom: 40,
  },
});
