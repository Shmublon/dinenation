import React, {useContext, useState} from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {useTranslation} from 'react-i18next';
import Center from '../basics/Center';
import BoldText from '../text/BoldText';
import VerticalSpace from '../basics/VerticalSpace';
import WhiteCard from '../cards/WhiteCard';
import DineSlider from '../slider/DineSlider';
import HorizontalTabs from '../tabs/HorizontalTabs';
import NormalText from '../text/NormalText';
import VerticalTabs from '../tabs/VerticalTabs';
import CalculatorFooter from './CalculatorFooter';
import {saveCalculatorData} from '../../store/actions';
import {context} from '../../store/context';
import {Target} from '../../enums/Target';
import {Intensity} from '../../enums/Intensity';

const initialWorkouts = 3;

interface Props {
  onBack: () => void;
  onNext: () => void;
}

const CalculatorTargetStep: React.FC<Props> = ({onNext, onBack}) => {
  const {t} = useTranslation();
  const {dispatch, state} = useContext(context);
  const [workouts, setWorkouts] = useState(initialWorkouts);
  const [target, setTarget] = useState(Target.LOSE);
  const [intensity, setIntensity] = useState(Intensity.INTENSELY);

  const getTargetText = () => {
    switch (target) {
      case Target.LOSE:
        return t('loseText');
      case Target.SUPPORT:
        return t('supportText');
      case Target.GAIN:
        return t('gainText');
      default:
        return '';
    }
  };

  return (
    <>
      <ScrollView
        style={styles.container}
        contentContainerStyle={{paddingBottom: 40}}
        showsVerticalScrollIndicator={false}>
        <Center style={styles.header}>
          <BoldText fontSize={18} text={t('yourTarget')} />
        </Center>
        <VerticalSpace space={6} />
        <BoldText fontSize={14} centerText text={t('whatIsYourTarget')} />
        <VerticalSpace space={12} />
        <WhiteCard padding={8}>
          <HorizontalTabs
            selectedIndex={target}
            onChange={(index) => setTarget(index)}
            values={[t('lose'), t('support'), t('gain')]}
          />
        </WhiteCard>
        <VerticalSpace space={12} />
        <NormalText
          style={styles.weightText}
          text={getTargetText()}
          fontSize={16}
        />
        <VerticalSpace space={24} />

        {target !== 1 && (
          <>
            <WhiteCard padding={8}>
              <VerticalSpace space={8} />
              <BoldText
                centerText
                fontSize={14}
                text={target === 0 ? t('loseWeightTemp') : t('gainWeightTemp')}
              />
              <VerticalSpace space={20} />
              <VerticalTabs
                onTabChange={setIntensity}
                values={[t('slow'), t('normal'), t('fast')]}
              />
            </WhiteCard>
            <VerticalSpace space={16} />
          </>
        )}

        <WhiteCard padding={16}>
          <BoldText centerText fontSize={14} text={t('workoutsNumber')} />
          <DineSlider onSlidingComplete={setWorkouts} value={initialWorkouts} />
        </WhiteCard>
      </ScrollView>
      <CalculatorFooter
        onBack={onBack}
        onNext={() => {
          dispatch(
            saveCalculatorData({
              // @ts-ignore
              ...state.calculatorData,
              target,
              workouts,
              intensity,
            }),
          );
          onNext();
        }}
      />
    </>
  );
};

export default CalculatorTargetStep;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },

  header: {
    height: 55,
  },

  weightText: {
    paddingHorizontal: 22,
  },
});
