import React from 'react';
import {StyleSheet} from 'react-native';
import {useTranslation} from 'react-i18next';
import WhiteButton from '../buttons/WhiteButton';
import GreenButton from '../buttons/GreenButton';
import Row from '../basics/Row';

interface Props {
  onBack: () => void;
  onNext: () => void;
}

const CalculatorFooter: React.FC<Props> = ({onBack, onNext}) => {
  const {t} = useTranslation();

  return (
    <Row style={styles.footerContainer}>
      <WhiteButton
        style={styles.footerButton}
        onPress={onBack}
        text={t('back')}
      />
      <GreenButton
        style={styles.footerButton}
        onPress={onNext}
        text={t('next')}
      />
    </Row>
  );
};

export default CalculatorFooter;

const styles = StyleSheet.create({
  footerContainer: {
    height: 80,
    paddingHorizontal: 16,
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  footerButton: {
    width: 140,
  },
});
