import React, {useCallback, useMemo, useState} from 'react';
import {FlatList, StyleSheet, useWindowDimensions, View} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import Center from '../basics/Center';
import {Fonts} from '../../constants/Fonts';
import BoldText from '../text/BoldText';

const ITEM_WIDTH = 20;
const ITEM_HEIGHT = 60;

interface RenderItemProps {
  item: number;
  index: number;
}

interface Props {
  theme: Theme;
  start: number;
  end: number;
  initialValue: number;
  onActiveIndexChange?: (activeIndex: number) => void;
}

const MeasuresSlider: React.FC<Props> = ({
  theme,
  initialValue,
  onActiveIndexChange,
  start,
  end,
}) => {
  const windowWidth = useWindowDimensions().width;
  const HALF_LIST = windowWidth / 2 - ITEM_WIDTH / 2 - 32;
  const [localActiveIndex, setLocalActiveIndex] = useState(initialValue);

  const renderItem = useCallback(({item}: RenderItemProps) => {
    return (
      <View
        style={{
          width: ITEM_WIDTH * 10,
          // borderWidth: 1,
          // borderColor: 'red',
          flexDirection: 'row',
          height: ITEM_HEIGHT,
          marginBottom: -20,
        }}>
        {[...Array(10)].map((x, i) => {
          const numberToShow = i + item * 10;
          const bigStick = numberToShow % 5 === 0;
          return (
            <View key={i.toString()} style={[styles.itemContainer]}>
              <View
                style={[
                  bigStick ? styles.bigStick : styles.smallStick,
                  {
                    backgroundColor: theme.textColor,
                  },
                ]}
              />
            </View>
          );
        })}
      </View>
    );
  }, []);

  const keyExtractor = useCallback((index) => index.toString(), []);
  const headerFooterComponent = useCallback(
    () => (
      <View style={[styles.listHeaderFooterContainer, {width: HALF_LIST}]}>
        {[...Array(7)].map((x, i) => {
          return (
            <View key={i.toString()} style={[styles.itemContainer]}>
              <View
                style={[
                  i === 2 ? styles.bigStick : styles.smallStick,
                  {backgroundColor: theme.textColor},
                ]}
              />
            </View>
          );
        })}
      </View>
    ),
    [],
  );

  const list = useMemo(() => {
    const items: number[] = [];
    for (let i = start; i < end; i++) {
      if (i % 10 === 0) {
        items.push(i / 10);
      }
    }
    return (
      <FlatList
        ListHeaderComponent={headerFooterComponent}
        ListFooterComponent={headerFooterComponent}
        horizontal
        contentContainerStyle={[styles.listContentStyle]}
        onMomentumScrollEnd={(e) =>
          onActiveIndexChange!(
            Math.round(e.nativeEvent.contentOffset.x / ITEM_WIDTH + start),
          )
        }
        onScroll={(event) => {
          const value =
            Math.round(event.nativeEvent.contentOffset.x / ITEM_WIDTH) + start;
          if (value >= start && value < end) {
            setLocalActiveIndex(value);
          }
        }}
        initialScrollIndex={(initialValue - start) / 10}
        scrollEventThrottle={16}
        showsHorizontalScrollIndicator={false}
        renderItem={renderItem}
        data={items}
        snapToOffsets={[...Array(end - start)].map((x, i) => i * ITEM_WIDTH)}
        getItemLayout={(data, index) => ({
          length: ITEM_WIDTH * 10,
          offset: ITEM_WIDTH * index * 10,
          index,
        })}
        keyExtractor={keyExtractor}
      />
    );
  }, [start, end, theme]);

  return (
    <Center>
      {list}
      <Center
        style={{
          position: 'absolute',
          width: 40,
          height: 20,
          top: -15,
        }}>
        <BoldText
          text={localActiveIndex.toString()}
          fontSize={16}
          color={theme.accentColor}
        />
      </Center>
      <View
        style={[styles.activeStick, {backgroundColor: theme.accentColor}]}
      />
    </Center>
  );
};

export default withTheme(MeasuresSlider);

const styles = StyleSheet.create({
  container: {
    paddingVertical: 12,
  },
  itemContainer: {
    height: ITEM_HEIGHT,
    width: ITEM_WIDTH,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listHeaderFooterContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: -20,
  },
  listContentStyle: {
    alignItems: 'flex-end',
  },
  bigStick: {
    width: 2,
    height: 25,
  },
  smallStick: {
    width: 1,
    height: 15,
  },
  stubStick: {
    width: 1,
    height: 15,
    opacity: 0.5,
  },
  activeStick: {
    width: 2,
    height: 30,
    position: 'absolute',
    bottom: 0,
  },
  text: {
    fontFamily: Fonts.regular,
    fontWeight: '400',
    fontSize: 16,
  },
  activeText: {
    fontFamily: Fonts.regular,
    fontWeight: '700',
    fontSize: 18,
  },
});
