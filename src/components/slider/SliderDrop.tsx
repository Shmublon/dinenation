import React from 'react';
import Svg, {G, Path} from 'react-native-svg';
import {StyleProp, ViewStyle} from 'react-native';

interface Props {
  readonly size: number;
  readonly style?: StyleProp<ViewStyle>;
  readonly color: string;
}

const SliderDrop: React.FC<Props> = ({size, style, color}) => {
  return (
    <Svg width={(36 * size) / 100} height={(42 * size) / 100} style={style}>
      <G scale={size / 100}>
        <Path
          d="M5.25426 5.24205C12.2428 -1.74735 23.5747 -1.74735 30.5632 5.24205C37.5526 12.2306 37.5526 23.5625 30.5632 30.551L20.4393 40.6749C19.0416 42.0726 16.7759 42.0726 15.3782 40.6749L5.25426 30.551C-1.73514 23.5625 -1.73514 12.2306 5.25426 5.24205Z"
          fill={color}
        />
      </G>
    </Svg>
  );
};

export default SliderDrop;
