import React, {useState} from 'react';
import {Theme, withTheme} from '../../Theming';
import {StyleSheet, useWindowDimensions, View} from 'react-native';
import NormalText from '../text/NormalText';
import Row from '../basics/Row';
import SliderDrop from './SliderDrop';
import {Slider} from '@miblanchard/react-native-slider';
import BoldText from '../text/BoldText';
import Center from '../basics/Center';

interface Props {
  theme: Theme;
  value: number;
  onSlidingComplete: (index: number) => void;
}

const DineSlider: React.FC<Props> = ({theme, value, onSlidingComplete}) => {
  const values: number[] = [...Array(8).keys()];
  const windowWidth = useWindowDimensions().width;
  const sliderWidth = windowWidth - 65;
  const [localValue, setLocalValue] = useState(value);

  return (
    <View style={styles.wrapper}>
      <Row style={[styles.numbersContainer, {width: sliderWidth}]}>
        {values.map((value) => {
          return (
            <View key={value.toString()} style={styles.stickContainer}>
              <View
                style={[styles.stick, {backgroundColor: theme.textColor}]}
              />
            </View>
          );
        })}
      </Row>
      <Slider
        renderThumbComponent={() => (
          <View
            style={[styles.innerThumb, {backgroundColor: theme.accentColor}]}
          />
        )}
        value={localValue}
        renderAboveThumbComponent={() => (
          <Center style={styles.dropsContainer}>
            <SliderDrop size={100} color={theme.accentColor} />
            <View style={styles.dropContainer}>
              <BoldText
                fontSize={14}
                text={localValue.toString()}
                color={theme.primaryColor}
              />
            </View>
          </Center>
        )}
        onValueChange={(newValue) => setLocalValue(newValue[0])}
        onSlidingComplete={(newValue) => onSlidingComplete(newValue[0])}
        containerStyle={[styles.container, {width: sliderWidth}]}
        step={1}
        thumbTouchSize={{
          height: 120,
          width: 10,
        }}
        trackStyle={styles.trackStyle}
        minimumValue={0}
        maximumValue={7}
        minimumTrackTintColor={theme.accentColor}
        maximumTrackTintColor={theme.inactive2}
        thumbTintColor={theme.accentColor}
      />
      <Row style={styles.numbersContainer}>
        {values.map((value) => {
          return (
            <View key={value.toString()} style={styles.number}>
              <NormalText text={value.toString()} fontSize={16} />
            </View>
          );
        })}
      </Row>
    </View>
  );
};

export default withTheme(DineSlider);

const styles = StyleSheet.create({
  wrapper: {
    height: 100,
    paddingTop: 40,
  },
  container: {
    height: 33,
  },
  numbersContainer: {
    justifyContent: 'space-between',
  },
  dropsContainer: {
    position: 'absolute',
    bottom: -2,
    left: -9,
  },
  number: {
    height: 24,
    width: 18,
    alignItems: 'center',
  },

  stick: {
    height: 11,
    width: 1,
  },

  stickContainer: {
    width: 18,
    alignItems: 'center',
  },

  dropContainer: {
    position: 'absolute',
    paddingBottom: 4,
  },

  trackStyle: {
    height: 2,
  },

  innerThumb: {
    height: 18,
    width: 18,
    borderRadius: 9,
  },
});
