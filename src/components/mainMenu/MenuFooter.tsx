import React from 'react';
import {StyleProp, StyleSheet, View, ViewStyle} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import Row from '../basics/Row';
import BoldText from '../text/BoldText';
import GreenButton from '../buttons/GreenButton';
import {useTranslation} from 'react-i18next';
import VerticalSpace from '../basics/VerticalSpace';

interface Props {
  readonly style?: StyleProp<ViewStyle>;
  theme: Theme;
  onOrderPress: () => void;
}

const MenuFooter: React.FC<Props> = ({style, theme, onOrderPress}) => {
  const {t} = useTranslation();

  return (
    <View
      style={[style, styles.container, {backgroundColor: theme.primaryColor}]}>
      <Row style={styles.textContainer}>
        <BoldText
          color={theme.secondaryColor}
          fontSize={18}
          text={'На неделю 7дней (Демо)'}
        />
        <BoldText color={theme.secondaryColor} fontSize={18} text={'€ 330'} />
      </Row>
      <VerticalSpace space={16} />
      <GreenButton text={t('orderButton')} onPress={onOrderPress} />
    </View>
  );
};

export default withTheme(MenuFooter);

const styles = StyleSheet.create({
  container: {
    height: 120,
    width: '100%',
    padding: 16,
    position: 'absolute',
    bottom: 0,
  },

  textContainer: {
    justifyContent: 'space-between',
  },
});
