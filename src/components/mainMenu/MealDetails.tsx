import React from 'react';
import {StyleProp, StyleSheet, View, ViewStyle} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import BoldText from '../text/BoldText';
import {useTranslation} from 'react-i18next';
import VerticalSpace from '../basics/VerticalSpace';
import WhiteCard from '../cards/WhiteCard';
import NormalText from '../text/NormalText';
import Row from '../basics/Row';
import ProteinIcon from '../icons/ProteinIcon';
import FatIcon from '../icons/FatIcon';
import CarbonIcon from '../icons/CarbonIcon';
import GreenFlameIcon from '../icons/GreenFlameIcon';

interface Props {
  readonly style?: StyleProp<ViewStyle>;
  theme: Theme;
}

const MealDetails: React.FC<Props> = ({style, theme}) => {
  const {t} = useTranslation();

  const row = (
    icon: Element,
    value: string,
    amount: string,
    name: string,
    color: string,
  ) => {
    return (
      <>
        <View style={[styles.separator, {backgroundColor: theme.textColor}]} />
        <Row style={styles.row}>
          {icon}
          <BoldText
            style={styles.value}
            fontSize={16}
            text={value}
            color={color}
          />
          <NormalText
            style={styles.amount}
            fontSize={16}
            text={amount}
            color={color}
          />
          <BoldText
            style={styles.name}
            fontSize={16}
            text={t(name)}
            color={color}
          />
        </Row>
      </>
    );
  };

  return (
    <View
      style={[
        style,
        styles.container,
        {backgroundColor: theme.backgroundColor},
      ]}>
      <VerticalSpace space={10} />
      <BoldText centerText fontSize={16} text={t('mealDetailsHeaderText')} />
      <VerticalSpace space={10} />
      <WhiteCard style={styles.innerContainer} padding={0}>
        <VerticalSpace space={16} />
        <NormalText
          style={styles.subHeader}
          fontSize={16}
          text={t('mealDetailsSubHeaderText')}
        />
        <VerticalSpace space={16} />
        {row(
          <ProteinIcon size={100} />,
          '45',
          'Мало',
          'proteins',
          theme.lightGreenColor,
        )}
        {row(
          <FatIcon size={100} />,
          '34',
          'Умеренно',
          'fats',
          theme.yellowColor,
        )}
        {row(
          <CarbonIcon size={100} />,
          '56',
          'Много',
          'carbons',
          theme.accentColor,
        )}
        {row(
          <GreenFlameIcon size={100} />,
          '1500',
          'Норма',
          'calories',
          theme.secondaryColor,
        )}
      </WhiteCard>
    </View>
  );
};

export default withTheme(MealDetails);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },
  innerContainer: {
    height: 400,
  },
  subHeader: {
    paddingHorizontal: 16,
  },
  row: {
    height: 40,
    paddingHorizontal: 32,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  value: {
    width: '15%',
  },
  name: {
    width: '32%',
  },
  amount: {
    width: '38%',
  },
  separator: {
    width: '100%',
    height: 1,
    opacity: 0.1,
  },
});
