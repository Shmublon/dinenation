import React, {useMemo, useRef} from 'react';
import Animated from 'react-native-reanimated';
import {FlatList, StyleSheet, View} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import DishCard from './DishCard';
import {IDish} from '../../interfaces/IDish';
import NormalText from '../text/NormalText';

const ITEM_WIDTH = 144;
const SPACING = 8;

interface RenderItemProps {
  item: IDish;
  index: number;
}

interface Props {
  theme: Theme;
  dishes?: IDish[];
  addedDishes: IDish[];
  setAddedDishes: (addedDishes: IDish[]) => void;
  onDishPress: (dish: IDish) => void;
  sliderLabel: string;
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const DishSlider: React.FC<Props> = ({
  theme,
  onDishPress,
  dishes,
  addedDishes,
  setAddedDishes,
  sliderLabel,
}) => {
  const scrollX = useRef(new Animated.Value(0)).current;

  const renderItem = ({item, index}: RenderItemProps) => {
    const inputRange = [
      (index - 3) * (ITEM_WIDTH + SPACING),
      (index - 1.5) * (ITEM_WIDTH + SPACING),
      (index - 1) * (ITEM_WIDTH + SPACING),
      index * (ITEM_WIDTH + SPACING),
      (index + 1) * (ITEM_WIDTH + SPACING),
    ];

    const scale = scrollX.interpolate({
      inputRange,
      outputRange: [0.7, 1, 1, 1, 0.7],
    });
    return (
      <Animated.View style={{transform: [{scale}]}}>
        <DishCard
          price={item.price}
          style={styles.dishContainer}
          imageUrl={item.imageUrl}
          isAdded={addedDishes.includes(item)}
          calories={item.calories}
          name={item.name}
          shortDescription={item.shortDescription}
          onAddPress={() => {
            if (addedDishes.indexOf(item) === -1) {
              setAddedDishes([...addedDishes, item]);
            } else {
              addedDishes.splice(addedDishes.indexOf(item), 1);
              setAddedDishes([...addedDishes]);
            }
          }}
          onPress={() => onDishPress(item)}
        />
      </Animated.View>
    );
  };

  const list = useMemo(() => {
    return (
      <AnimatedFlatList
        horizontal
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {useNativeDriver: true},
        )}
        style={styles.listContainerStyle}
        contentContainerStyle={styles.contentContainerStyle}
        showsHorizontalScrollIndicator={false}
        renderItem={renderItem}
        data={dishes}
        getItemLayout={(data: any, index: number) => ({
          length: ITEM_WIDTH,
          offset: ITEM_WIDTH * index,
          index,
        })}
        keyExtractor={(item: IDish, index: number) => index.toString()}
      />
    );
  }, [theme, addedDishes, dishes]);

  return (
    <View>
      <NormalText style={styles.labelStyle} fontSize={16} text={sliderLabel} />
      {list}
    </View>
  );
};

export default withTheme(DishSlider);

const styles = StyleSheet.create({
  contentContainerStyle: {
    paddingHorizontal: 16,
  },
  listContainerStyle: {
    flexGrow: 0,
  },
  labelStyle: {
    marginLeft: 20,
    marginBottom: 12,
  },
  dishContainer: {
    marginRight: SPACING,
  },
});
