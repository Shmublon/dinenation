import React from 'react';
import {StyleProp, StyleSheet, View, ViewStyle} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import BoldText from '../text/BoldText';
import {useTranslation} from 'react-i18next';
import VerticalSpace from '../basics/VerticalSpace';
import WhiteCard from '../cards/WhiteCard';
import VerticalTab from '../tabs/VerticalTab';
import {Duration} from '../../enums/Duration';
import NormalText from '../text/NormalText';
import Row from '../basics/Row';

interface Props {
  readonly style?: StyleProp<ViewStyle>;
  theme: Theme;
  selectedValue: Duration;
  onTabPress: (duration: Duration) => void;
}

const DurationDetails: React.FC<Props> = ({
  style,
  theme,
  selectedValue,
  onTabPress,
}) => {
  const {t} = useTranslation();

  const rightComponent = (price: string, name: string) => {
    return (
      <Row
        style={[
          styles.rightComponentContainer,
          {backgroundColor: theme.primaryColor},
        ]}>
        <BoldText fontSize={16} text={price + '€'} />
        <BoldText color={theme.accentColor} fontSize={16} text={name} />
      </Row>
    );
  };

  return (
    <View
      style={[
        style,
        styles.container,
        {backgroundColor: theme.backgroundColor},
      ]}>
      <VerticalSpace space={10} />
      <BoldText centerText fontSize={16} text={t('durationDetailsHeaderText')} />
      <VerticalSpace space={10} />
      <WhiteCard style={styles.innerContainer} padding={8}>
        <VerticalTab
          rightComponent={rightComponent('70', 'Демо проба')}
          onPress={() => onTabPress(Duration.WEEK)}
          title={t('week')}
          isActive={selectedValue === Duration.WEEK}
        />
        <NormalText
          style={styles.description}
          fontSize={16}
          text={t('weekDescription')}
        />
        <VerticalTab
          rightComponent={rightComponent('150', '15% Скидка')}
          onPress={() => onTabPress(Duration.MONTH)}
          title={t('month')}
          isActive={selectedValue === Duration.MONTH}
        />
        <NormalText
          style={styles.description}
          fontSize={16}
          text={t('monthDescription')}
        />
        <VerticalTab
          rightComponent={rightComponent('300', '25% Скидка')}
          onPress={() => onTabPress(Duration.THREE_MONTH)}
          title={t('threeMonths')}
          isActive={selectedValue === Duration.THREE_MONTH}
        />
        <NormalText
          style={styles.description}
          fontSize={16}
          text={t('threeMonthsDescription')}
        />
        <VerticalTab
          rightComponent={rightComponent('500', '30% Скидка')}
          onPress={() => onTabPress(Duration.SIX_MONTH)}
          title={t('sixMonths')}
          isActive={selectedValue === Duration.SIX_MONTH}
        />
        <NormalText
          style={styles.description}
          fontSize={16}
          text={t('sixMonthsDescription')}
        />
      </WhiteCard>
    </View>
  );
};

export default withTheme(DurationDetails);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },
  innerContainer: {
    height: 650,
  },
  description: {
    paddingHorizontal: 8,
    paddingTop: 8,
    paddingBottom: 16,
  },
  rightComponentContainer: {
    justifyContent: 'space-between',
    height: 23,
    width: 170,
    position: 'absolute',
    right: 8,
    borderRadius: 20,
    paddingHorizontal: 8,
  },
});
