import React from 'react';
import {
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  useWindowDimensions,
  ViewStyle,
} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import Row from '../basics/Row';
import FilterIcon from '../icons/FilterIcon';
import HorizontalSpace from '../basics/HorizontalSpace';
import BoldText from '../text/BoldText';
import Center from '../basics/Center';
import NormalText from '../text/NormalText';

interface Props {
  readonly style?: StyleProp<ViewStyle>;
  theme: Theme;
  onInfoPress: () => void;
  onFilterPress: () => void;
  filterCounter?: number;
  eatTimesPerDay: number;
  calories: number;
  proteins: number;
  fats: number;
  carbons: number;
}

const MenuHeader: React.FC<Props> = ({
  style,
  theme,
  onInfoPress,
  onFilterPress,
  filterCounter,
  eatTimesPerDay,
  calories,
  proteins,
  fats,
  carbons,
}) => {
  const windowWidth = useWindowDimensions().width;

  return (
    <Row style={[style, styles.container, {width: windowWidth - 32}]}>
      <TouchableOpacity
        style={[styles.infoContainer, {backgroundColor: theme.primaryColor}]}
        activeOpacity={1}
        onPress={onInfoPress}>
        <Row>
          <BoldText
            color={theme.secondaryColor}
            fontSize={14}
            text={eatTimesPerDay.toString()}
          />
          <NormalText style={styles.text} fontSize={14} text={'приема'} />
        </Row>
        <Row>
          <BoldText
            color={theme.secondaryColor}
            fontSize={14}
            text={proteins.toString()}
          />
          <NormalText style={styles.text} fontSize={14} text={'Б'} />
        </Row>
        <Row>
          <BoldText
            color={theme.secondaryColor}
            fontSize={14}
            text={fats.toString()}
          />
          <NormalText style={styles.text} fontSize={14} text={'Ж'} />
        </Row>
        <Row>
          <BoldText
            color={theme.secondaryColor}
            fontSize={14}
            text={carbons.toString()}
          />
          <NormalText style={styles.text} fontSize={14} text={'У'} />
        </Row>
        <Row>
          <BoldText
            color={theme.secondaryColor}
            fontSize={14}
            text={calories.toString()}
          />
          <NormalText style={styles.text} fontSize={14} text={'Ккал'} />
        </Row>
      </TouchableOpacity>
      <HorizontalSpace space={12} />
      <TouchableOpacity
        style={[
          styles.filerContainer,
          {backgroundColor: theme.backgroundColor},
        ]}
        activeOpacity={1}
        onPress={onFilterPress}>
        <FilterIcon size={100} color={theme.textColor} />
        <Center
          style={[
            styles.filterCounterContainer,
            {backgroundColor: theme.accentColor},
          ]}>
          {filterCounter && (
            <BoldText
              fontSize={14}
              color={theme.primaryColor}
              text={filterCounter.toString()}
            />
          )}
        </Center>
      </TouchableOpacity>
    </Row>
  );
};

export default withTheme(MenuHeader);

const styles = StyleSheet.create({
  container: {
    height: 54,
    left: 16,
    alignItems: 'center',
  },
  infoContainer: {
    flexDirection: 'row',
    flex: 1,
    height: 54,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.25)',
    alignItems: 'center',
    paddingHorizontal: 16,
    justifyContent: 'space-between',
  },
  filerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 43,
    height: 43,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.25)',
  },
  filterCounterContainer: {
    width: 16,
    height: 16,
    borderRadius: 8,
    position: 'absolute',
    right: 3,
    bottom: 4,
  },
  text: {
    marginLeft: 4,
  },
});
