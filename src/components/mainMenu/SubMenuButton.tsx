import React from 'react';
import {
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import ArrowDownIcon from '../icons/ArrowDownIcon';
import Row from '../basics/Row';
import NormalText from '../text/NormalText';
import BoldText from '../text/BoldText';

interface Props {
  readonly style?: StyleProp<ViewStyle>;
  theme: Theme;
  icon: () => Element;
  mainText: string;
  secondaryText: string;
  onPress?: () => void;
}

const SubMenuButton: React.FC<Props> = ({
  style,
  theme,
  icon,
  mainText,
  secondaryText,
  onPress,
}) => {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={1} style={[style, styles.container]}>
      <View
        style={[
          styles.iconContainer,
          {backgroundColor: theme.backgroundColor},
        ]}>
        {icon()}
      </View>
      <View style={styles.textsContainer}>
        <Row>
          <BoldText fontSize={14} text={mainText} />
          <ArrowDownIcon
            style={styles.arrow}
            size={100}
            color={theme.inactiveColor}
          />
        </Row>
        <NormalText fontSize={12} text={secondaryText} />
      </View>
    </TouchableOpacity>
  );
};

export default withTheme(SubMenuButton);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 70,
    width: '50%',
    paddingHorizontal: 16,
    alignItems: 'center',
  },
  iconContainer: {
    width: 42,
    height: 42,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrow: {
    marginLeft: 16,
    marginTop: 4,
  },
  textsContainer: {
    marginLeft: 8,
    height: 38,
    justifyContent: 'space-between',
  },
});
