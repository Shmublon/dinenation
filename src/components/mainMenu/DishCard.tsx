import React from 'react';
import {
  Image,
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import Center from '../basics/Center';
import CheckInCircleIcon from '../icons/CheckInCircleIcon';
import PlusInCircleIcon from '../icons/PlusInCircleIcon';
import NormalText from '../text/NormalText';
import VerticalSpace from '../basics/VerticalSpace';
import Row from '../basics/Row';
import FlameIcon from '../icons/FlameIcon';
import HorizontalSpace from '../basics/HorizontalSpace';
import BoldText from '../text/BoldText';

interface Props {
  readonly style?: StyleProp<ViewStyle>;
  theme: Theme;
  onAddPress: () => void;
  onPress: () => void;
  isAdded: boolean;
  name: string;
  shortDescription: string;
  calories: number;
  imageUrl: string;
  price: number;
}

const DISH_SIZE = 112;
const DishCard: React.FC<Props> = ({
  style,
  theme,
  onAddPress,
  onPress,
  isAdded,
  name,
  shortDescription,
  calories,
  imageUrl,
  price,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={onPress}
      style={[
        style,
        styles.container,
        {backgroundColor: theme.backgroundColor},
      ]}>
      {isAdded && (
        <View
          style={[styles.underBack, {backgroundColor: theme.lightGreenColor}]}
        />
      )}
      <View style={[styles.upperBack, {backgroundColor: theme.primaryColor}]}>
        <Row style={styles.namePrice}>
          <NormalText numberOfLines={1} fontSize={14} text={name} />
          <BoldText fontSize={14} text={'€' + price.toString()} />
        </Row>
        <VerticalSpace space={4} />
        <NormalText
          numberOfLines={1}
          centerText
          fontSize={12}
          text={shortDescription}
          color={theme.inactiveColor}
        />
        <VerticalSpace space={8} />
        <Row style={styles.calories}>
          <FlameIcon size={100} color={theme.accentColor} />
          <HorizontalSpace space={8} />
          <NormalText
            centerText
            fontSize={12}
            color={theme.accentColor}
            text={calories.toString() + ' Ккал'}
          />
        </Row>
      </View>
      <Image style={styles.dishImage} source={{uri: imageUrl}} />
      <TouchableOpacity
        style={styles.addTouchableArea}
        activeOpacity={1}
        onPress={onAddPress}>
        <Center
          style={[styles.addContainer, {backgroundColor: theme.primaryColor}]}>
          {isAdded && (
            <CheckInCircleIcon size={100} color={theme.lightGreenColor} />
          )}
          {!isAdded && (
            <PlusInCircleIcon size={100} color={theme.secondaryColor} />
          )}
        </Center>
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

export default withTheme(DishCard);

const styles = StyleSheet.create({
  container: {
    height: 220,
    width: 144,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  namePrice: {
    justifyContent: 'space-between',
  },

  dishImage: {
    height: DISH_SIZE,
    width: DISH_SIZE,
  },

  addTouchableArea: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },

  addContainer: {
    height: 30,
    width: 30,
    borderRadius: 15,
  },

  underBack: {
    height: 173,
    width: 144,
    borderRadius: 20,
    position: 'absolute',
    bottom: 22,
  },

  calories: {
    justifyContent: 'center',
  },

  upperBack: {
    paddingHorizontal: 16,
    paddingTop: DISH_SIZE - 18,
    height: 173,
    width: 138,
    borderRadius: 20,
    position: 'absolute',
    bottom: 25,
  },
});
