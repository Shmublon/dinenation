import React from 'react';
import {StyleProp, StyleSheet, View, ViewStyle} from 'react-native';
import {Theme, withTheme} from '../../Theming';

interface Props {
  theme: Theme;
  padding: number;
  style?: StyleProp<ViewStyle>;
}

const WhiteCard: React.FC<Props> = ({children, theme, padding, style}) => {
  return (
    <View
      style={[
        styles.container,
        style,
        {
          backgroundColor: theme.primaryColor,
          padding: padding,
        },
      ]}>
      {children}
    </View>
  );
};

export default withTheme(WhiteCard);

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
  },
});
