import React from 'react';
import {Text, StyleSheet, StyleProp, TextStyle} from 'react-native';
import {withTheme} from '../../Theming';
import type {Theme} from '../../Theming';
import {Fonts} from '../../constants/Fonts';

interface Props {
  text: string;
  style?: StyleProp<TextStyle>;
  theme: Theme;
  centerText?: boolean;
  fontSize: number;
  color?: string;
}

const BoldText: React.FC<Props> = ({
  text,
  theme,
  style,
  centerText,
  fontSize,
  color,
}) => {
  const textAlign = centerText ? 'center' : 'auto';
  return (
    <Text
      style={[
        styles.text,
        style,
        {color: color ? color : theme.textColor, textAlign, fontSize},
      ]}>
      {text}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    fontFamily: Fonts.regular,
    fontWeight: '700',
  },
});

export default withTheme(BoldText);
