import React from 'react';
import {StyleProp, StyleSheet, View, ViewStyle} from 'react-native';
import type {Theme} from '../../Theming';
import Row from '../basics/Row';
import BoldText from './BoldText';
import {withTheme} from '../../Theming';
import CenterRow from '../basics/CenterRow';

interface Props {
  text: string;
  style?: StyleProp<ViewStyle>;
  theme: Theme;
}

const TextBullet: React.FC<Props> = ({text, style, theme}) => {
  return (
    <CenterRow style={style}>
      <View style={[styles.dot, {backgroundColor: theme.textColor}]} />
      <BoldText fontSize={14} text={text} />
    </CenterRow>
  );
};

const styles = StyleSheet.create({
  dot: {
    width: 5,
    height: 5,
    borderRadius: 2.5,
    marginRight: 4,
  },
});

export default withTheme(TextBullet);
