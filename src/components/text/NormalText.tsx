import React from 'react';
import {Text, StyleSheet, StyleProp, TextStyle} from 'react-native';
import {withTheme} from '../../Theming';
import type {Theme} from '../../Theming';
import {Fonts} from '../../constants/Fonts';

interface Props {
  text: string;
  style?: StyleProp<TextStyle>;
  theme: Theme;
  centerText?: boolean;
  fontSize: number;
  onPress?: () => void;
  color?: string;
  numberOfLines?: number;
}

const NormalText: React.FC<Props> = ({
  text,
  theme,
  style,
  centerText,
  fontSize,
  onPress,
  color,
  numberOfLines,
}) => {
  const textAlign = centerText ? 'center' : 'auto';
  return (
    <Text
      onPress={onPress}
      numberOfLines={numberOfLines}
      style={[
        styles.text,
        style,
        {
          color: color ? color : theme.textColor,
          textAlign,
          fontSize,
        },
      ]}>
      {text}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    fontFamily: Fonts.regular,
    fontWeight: '400',
  },
});

export default withTheme(NormalText);
