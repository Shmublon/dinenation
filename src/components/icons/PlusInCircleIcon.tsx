import React from 'react';
import Svg, {G, Path, Rect} from 'react-native-svg';
import {IconProps} from './IconProps';

const PlusInCircleIcon: React.FC<IconProps> = ({size, style, color}) => {
  return (
    <Svg width={(23 * size) / 100} height={(23 * size) / 100} style={style}>
      <G scale={size / 100}>
        <Path
          d="M11.5 0C5.1589 0 0 5.1589 0 11.5C0 17.8411 5.1589 23 11.5 23C17.8411 23 23 17.8411 23 11.5C23 5.1589 17.8411 0 11.5 0ZM11.5 20.9091C6.31176 20.9091 2.09091 16.6882 2.09091 11.5C2.09091 6.31183 6.31176 2.09091 11.5 2.09091C16.6882 2.09091 20.9091 6.31183 20.9091 11.5C20.9091 16.6882 16.6882 20.9091 11.5 20.9091Z"
          fill={color}
        />
        <Rect x="10" y="6" width="3" height="11" rx="1.5" fill={color} />
        <Rect
          x="17"
          y="10"
          width="3"
          height="11"
          rx="1.5"
          transform="rotate(90 17 10)"
          fill={color}
        />
      </G>
    </Svg>
  );
};

export default PlusInCircleIcon;
