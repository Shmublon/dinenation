import React from 'react';
import Svg, {G, Path} from 'react-native-svg';
import {IconProps} from './IconProps';

const ArrowDownIcon: React.FC<IconProps> = ({size, style, color}) => {
  return (
    <Svg width={(15 * size) / 100} height={(10 * size) / 100} style={style}>
      <G scale={size / 100}>
        <Path
          d="M0.743628 1.20538C0.144979 1.86049 0.201444 2.87686 0.833973 3.49798L6.17563 8.85089C6.49184 9.16709 6.90969 9.3252 7.32753 9.3252C7.74538 9.3252 8.16311 9.16709 8.47943 8.85089L13.821 3.50927C14.4421 2.88815 14.5099 1.86049 13.9113 1.21678C13.2789 0.527792 12.2062 0.505319 11.5511 1.16032L8.12923 4.58211C7.68891 5.02254 6.97733 5.02254 6.5369 4.58211L3.11508 1.16032C2.44878 0.494027 1.37593 0.516501 0.743628 1.20538Z"
          fill={color}
        />
      </G>
    </Svg>
  );
};

export default ArrowDownIcon;
