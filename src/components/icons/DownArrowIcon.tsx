import React from 'react';
import Svg, {G, Path} from 'react-native-svg';
import {IconProps} from './IconProps';

const HeaderBackIcon: React.FC<IconProps> = ({size, style, color}) => {
  return (
    <Svg width={(10 * size) / 100} height={(14 * size) / 100} style={style}>
      <G scale={size / 100}>
        <Path
          d="M8.79426 0.418189C8.13915 -0.18046 7.12277 -0.123995 6.50165 0.508534L1.14875 5.85019C0.832541 6.1664 0.674438 6.58425 0.674438 7.00209C0.674438 7.41994 0.832541 7.83767 1.14875 8.15399L6.49036 13.4955C7.11148 14.1167 8.13915 14.1844 8.78285 13.5859C9.47184 12.9535 9.49432 11.8807 8.83932 11.2256L5.41752 7.80379C4.97709 7.36347 4.97709 6.65189 5.41752 6.21146L8.83932 2.78964C9.50561 2.12334 9.48313 1.05049 8.79426 0.418189Z"
          fill={color}
        />
      </G>
    </Svg>
  );
};

export default HeaderBackIcon;
