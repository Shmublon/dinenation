import {StyleProp, ViewStyle} from 'react-native';

export interface IconProps {
  readonly size: number;
  readonly style?: StyleProp<ViewStyle>;
  readonly color?: string;
}
