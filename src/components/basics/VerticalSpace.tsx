import React from 'react';
import {View} from 'react-native';

interface Props {
  space: number;
}

const VerticalSpace: React.FC<Props> = ({space}) => {
  return <View style={{height: space}} />;
};

export default VerticalSpace;
