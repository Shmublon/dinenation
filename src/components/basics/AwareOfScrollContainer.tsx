import {
  ScrollView,
  StyleProp,
  useWindowDimensions,
  View,
  ViewStyle,
} from 'react-native';
import React from 'react';

interface Props {
  style?: StyleProp<ViewStyle>;
}

const AwareOfScrollContainer: React.FC<Props> = ({children, style}) => {
  const windowHeight = useWindowDimensions().height;

  if (windowHeight - 160 < 600) {
    return (
      <ScrollView
        style={style}
        contentContainerStyle={{paddingBottom: 40}}
        showsVerticalScrollIndicator={false}>
        {children}
      </ScrollView>
    );
  } else {
    return <View style={style}>{children}</View>;
  }
};

export default AwareOfScrollContainer;
