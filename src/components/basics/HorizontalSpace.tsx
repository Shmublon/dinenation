import React from 'react';
import {View} from 'react-native';

interface Props {
  space: number;
}

const HorizontalSpace: React.FC<Props> = ({space}) => {
  return <View style={{width: space}} />;
};

export default HorizontalSpace;
