import React from 'react';
import {
  SafeAreaView,
  StyleProp,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';
import {Theme, withTheme} from '../../Theming';

interface Props {
  style?: StyleProp<ViewStyle>;
  theme: Theme;
  safeAreaColor?: string;
}

const SafeContainer: React.FC<Props> = (props, theme) => {
  return (
    <>
      <SafeAreaView style={{flex: 0, backgroundColor: props.safeAreaColor}} />
      <SafeAreaView
        style={[styles.flex, {backgroundColor: theme.backgroundColor}]}>
        <View style={[styles.flex, props.style]}>{props.children}</View>
      </SafeAreaView>
      <SafeAreaView style={{flex: 0, backgroundColor: props.safeAreaColor}} />
    </>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
});

export default withTheme(SafeContainer);
