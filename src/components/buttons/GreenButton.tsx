import React from 'react';
import {Text, StyleSheet, Pressable, StyleProp, ViewStyle} from 'react-native';
import {ButtonState} from '../states/ButtonState';
import Center from '../basics/Center';
import {withTheme} from '../../Theming';
import type {Theme} from '../../Theming';
import {Fonts} from '../../constants/Fonts';

interface Props {
  text: string;
  state?: ButtonState;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
  theme: Theme;
}

const GreenButton: React.FC<Props> = ({
  text,
  state = ButtonState.ENABLED,
  onPress,
  theme,
  style,
}) => {
  return (
    <Pressable onPress={onPress} disabled={state === ButtonState.DISABLED}>
      <Center
        style={[styles.button, {backgroundColor: theme.secondaryColor}, style]}>
        <Text style={[styles.text, {color: theme.primaryColor}]}>{text}</Text>
      </Center>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  button: {
    width: '100%',
    height: 45,
    borderRadius: 10,
  },

  text: {
    fontFamily: Fonts.regular,
    fontSize: 16,
    fontWeight: '700',
  },
});

export default withTheme(GreenButton);
