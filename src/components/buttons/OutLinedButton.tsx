import React from 'react';
import {StyleSheet, Text, Pressable, StyleProp, ViewStyle} from 'react-native';
import {ButtonState} from '../states/ButtonState';
import Center from '../basics/Center';
import {withTheme} from '../../Theming';
import type {Theme} from '../../Theming';
import {Fonts} from '../../constants/Fonts';

interface Props {
  text: string;
  state?: ButtonState;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
  theme: Theme;
}

const OutLinedButton: React.FC<Props> = ({
  text,
  state = ButtonState.ENABLED,
  onPress,
  style,
  theme,
}) => {
  return (
    <Pressable onPress={onPress} disabled={state === ButtonState.DISABLED}>
      <Center style={[styles.button, style]}>
        <Text style={[styles.text, {color: theme.textColor}]}>{text}</Text>
      </Center>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  button: {
    width: '100%',
    height: 50,
  },

  text: {
    fontFamily: Fonts.regular,
    fontSize: 16,
    fontWeight: '700',
    textDecorationLine: 'underline',
  },
});

export default withTheme(OutLinedButton);
