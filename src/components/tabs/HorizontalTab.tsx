import React from 'react';
import {
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';

/**
 * A unit tab that is inside the tab control
 */
interface Props {
  title: string;
  textStyle: StyleProp<ViewStyle>;
  onPress: () => void;
  style: StyleProp<ViewStyle>;
}

const HorizontalTab: React.FC<Props> = ({title, style, textStyle, onPress}) => (
  <TouchableOpacity
    activeOpacity={1}
    style={[styles.tab, styles.touchableTab, style]}
    onPress={onPress}>
    <Text style={[styles.defaultText, textStyle]}>{title}</Text>
  </TouchableOpacity>
);

export default HorizontalTab;

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  touchableTab: {
    zIndex: 10,
  },
  defaultText: {
    color: '#DDD',
  },
});
