import React from 'react';
import {
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import Row from '../basics/Row';
import NormalText from '../text/NormalText';
import BoldText from '../text/BoldText';

interface Props {
  theme: Theme;
  values: string[];
  onItemPress: (index: number) => void;
  selectedIndex?: number;
  style?: StyleProp<ViewStyle>;
}

const WeekDaysTabs: React.FC<Props> = ({
  theme,
  values,
  selectedIndex,
  onItemPress,
  style,
}) => {
  return (
    <Row style={[styles.container, style]}>
      {values.map((value, index) => {
        if (selectedIndex === index) {
          return (
            <View
              key={index.toString()}
              style={[
                styles.itemContainer,
                {backgroundColor: theme.accentColor},
              ]}>
              <View
                style={[styles.activeTab, {backgroundColor: theme.accentColor}]}
              />
              <BoldText fontSize={15} text={value} color={theme.primaryColor} />
            </View>
          );
        }
        return (
          <TouchableOpacity
            key={index.toString()}
            activeOpacity={1}
            onPress={() => onItemPress(index)}
            style={[
              styles.itemContainer,
              {backgroundColor: theme.backgroundColor},
            ]}>
            <NormalText fontSize={16} text={value} />
          </TouchableOpacity>
        );
      })}
    </Row>
  );
};

export default withTheme(WeekDaysTabs);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemContainer: {
    width: 39,
    height: 51,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activeTab: {
    width: 44,
    height: 58,
    borderRadius: 25,
    position: 'absolute',
    opacity: 0.3,
  },
});
