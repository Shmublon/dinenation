import React from 'react';
import {
  Animated,
  Easing,
  LayoutChangeEvent,
  StyleSheet,
  View,
} from 'react-native';
import HorizontalTab from './HorizontalTab';

interface Props {
  /**
   * HorizontalTab values that are rendered on the view itself.
   */
  values: string[];

  /**
   * To enable/disable the tab control. Default value is `false`.
   */
  disable?: boolean;

  /**
   * A callback function of tab index on change. Changed index is send on the callback as a param.
   */
  onChange: (index: number) => void;

  /**
   * Index of the selected tab
   */
  selectedIndex: number;

  /**
   * Active HorizontalTab's offset height. Basically a padding.
   */
  offsetHeight: number;

  /**
   * Styles props of main wrapper
   */
  style?: {};

  /**
   * Styles props of tab control
   */
  tabControlStyle?: {};

  /**
   * Styles props of active tab
   */
  activeTabStyle?: {};

  /**
   * Selected HorizontalTab text style.
   */
  selectedTextStyle?: {};

  /**
   * Unselected HorizontalTab text style.
   */
  unSelectedTextStyle?: {};
}

interface State {
  selectedIndex: number;
  tabDimension: {width: number; height: number};
  activeTabPosition: {x: number; y: number};
  positionAnimationValue: Animated.Value;
}

class AnimatedTabs extends React.Component<Props, State> {
  public static defaultProps = {
    offsetHeight: 3,
    selectedIndex: 0,
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      selectedIndex: props.selectedIndex,
      tabDimension: {width: 0, height: 0},
      activeTabPosition: {x: props.offsetHeight, y: props.offsetHeight},
      positionAnimationValue: new Animated.Value(0),
    };
  }

  /**
   * On tab change event.
   *
   * @param {Number} index
   */
  onTabSelection = (index: number) => {
    const animate = () => {
      Animated.timing(this.state.positionAnimationValue, {
        toValue: this.state.activeTabPosition.x,
        duration: 150,
        easing: Easing.ease,
        useNativeDriver: true,
      }).start(() => this.props.onChange(index));
    };

    this.setState(
      (prevState) => ({
        selectedIndex: index,
        activeTabPosition: {
          x: prevState.tabDimension.width * index + this.props.offsetHeight,
          y: prevState.activeTabPosition.y,
        },
      }),
      animate,
    );
  };

  /**
   * Invoked on mount and layout change of `tabContainer` view.
   *
   * @param {Object} event
   */
  tabOnLayout = (event: LayoutChangeEvent) => {
    const {width, height} = event.nativeEvent.layout;
    const tabWidth =
      (width - this.props.offsetHeight * 2) / this.props.values.length;

    const animate = () => {
      Animated.timing(this.state.positionAnimationValue, {
        toValue: tabWidth * this.state.selectedIndex + this.props.offsetHeight,
        duration: 100,
        useNativeDriver: true,
      }).start();
    };

    this.setState(
      () => ({
        tabDimension: {width: tabWidth, height},
      }),
      animate,
    );
  };

  render() {
    const {
      style,
      disable,
      activeTabStyle,
      tabControlStyle,
      selectedTextStyle,
      unSelectedTextStyle,
    } = this.props;
    const {width, height} = this.state.tabDimension;
    const tabHeight = height - this.props.offsetHeight * 2;

    const isDisabled = disable ? 'none' : 'auto';
    const extraStyles = disable ? styles.vivid : {};

    return (
      <View style={[styles.mainContainer, style]} pointerEvents={isDisabled}>
        <View
          style={[
            styles.tabContainer,
            extraStyles,
            {height, borderRadius: height},
            tabControlStyle,
          ]}
          onLayout={this.tabOnLayout}>
          {this.props.values.map((tab, index) => (
            <HorizontalTab
              key={index}
              style={{height: tabHeight}}
              title={tab}
              textStyle={
                index !== this.state.selectedIndex
                  ? unSelectedTextStyle
                  : {...styles.activeText, ...selectedTextStyle}
              }
              onPress={() => this.onTabSelection(index)}
            />
          ))}
          <Animated.View
            style={[
              {
                width,
                height: tabHeight,
                left: 0,
                transform: [{translateX: this.state.positionAnimationValue}],
                top: this.state.activeTabPosition.y,
              },
              styles.tab,
              styles.activeTab,
              activeTabStyle,
            ]}
          />
        </View>
      </View>
    );
  }
}

export default AnimatedTabs;

const styles = StyleSheet.create({
  mainContainer: {
    height: 80,
    padding: 12,
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E5EAF2',
  },
  tabContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activeTab: {
    flex: 1,
    zIndex: 5,
    borderRadius: 40,
    position: 'absolute',
    backgroundColor: '#FFBA0D',
  },
  touchableTab: {
    zIndex: 10,
  },
  animatedView: {
    zIndex: 5,
    position: 'absolute',
  },
  defaultText: {
    color: '#DDD',
  },
  activeText: {
    color: '#FFFFFF',
  },
  vivid: {
    opacity: 0.7,
  },
});
