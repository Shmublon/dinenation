import React, {useMemo, useState} from 'react';
import {View} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import VerticalTab from './VerticalTab';

interface Props {
  theme: Theme;
  onTabChange?: (index: number) => void;
  values: string[];
}

const VerticalTabs: React.FC<Props> = ({theme, onTabChange, values}) => {
  const [activeIndex, setActiveIndex] = useState(0);

  return useMemo(
    () => (
      <View style={{backgroundColor: theme.primaryColor}}>
        {values.map((value: string, index: number) => {
          return (
            <VerticalTab
              key={index.toString()}
              onPress={() => {
                setActiveIndex(index);
                onTabChange!(index);
              }}
              style={{marginBottom: index !== values.length - 1 ? 8 : 0}}
              title={value}
              isActive={activeIndex === index}
            />
          );
        })}
      </View>
    ),
    [activeIndex, theme, values],
  );
};

export default withTheme(VerticalTabs);
