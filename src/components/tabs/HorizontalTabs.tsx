import React from 'react';
import {StyleSheet} from 'react-native';
import AnimatedTabs from './AnimatedTabs';
import {Fonts} from '../../constants/Fonts';
import {Theme, withTheme} from '../../Theming';

interface Props {
  theme: Theme;
  onChange: (index: number) => void;
  values: string[];
  selectedIndex?: number;
}

const HorizontalTabs: React.FC<Props> = ({
  theme,
  onChange,
  values,
  selectedIndex,
}) => {
  return (
    <AnimatedTabs
      activeTabStyle={[
        styles.activeTabStyle,
        {backgroundColor: theme.accentColor},
      ]}
      offsetHeight={0}
      style={[styles.mainWrapper, {backgroundColor: theme.primaryColor}]}
      unSelectedTextStyle={[
        styles.unSelectedTextStyle,
        {color: theme.inactiveColor},
      ]}
      selectedIndex={selectedIndex}
      selectedTextStyle={styles.selectedTextStyle}
      onChange={onChange}
      values={values}
    />
  );
};

export default withTheme(HorizontalTabs);

const styles = StyleSheet.create({
  mainWrapper: {
    padding: 0,
    borderRadius: 10,
    height: 45,
  },

  unSelectedTextStyle: {
    fontFamily: Fonts.regular,
    fontSize: 16,
    fontWeight: '400',
  },

  selectedTextStyle: {
    fontFamily: Fonts.regular,
    fontSize: 16,
    fontWeight: '700',
  },

  activeTabStyle: {
    borderRadius: 10,
  },
});
