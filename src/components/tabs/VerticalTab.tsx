import React from 'react';
import {
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
  Text,
} from 'react-native';
import {Theme, withTheme} from '../../Theming';
import CheckInCircleIcon from '../icons/CheckInCircleIcon';
import {Fonts} from '../../constants/Fonts';

/**
 * A unit tab that is inside the tab control
 */
interface Props {
  title: string;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
  isActive: boolean;
  theme: Theme;
  key?: string;
  rightComponent?: Element;
}

const VerticalTab: React.FC<Props> = ({
  title,
  style,
  theme,
  onPress,
  isActive,
  key,
  rightComponent,
}) => (
  <TouchableOpacity
    key={key}
    activeOpacity={1}
    style={[
      styles.container,
      style,
      {backgroundColor: isActive ? theme.accentColor : theme.backgroundColor},
    ]}
    onPress={onPress}>
    <View style={[styles.iconContainer, {backgroundColor: theme.primaryColor}]}>
      <CheckInCircleIcon
        size={100}
        color={isActive ? theme.accentColor : theme.inactiveColor}
      />
    </View>
    <Text
      style={[
        styles.text,
        {color: isActive ? theme.primaryColor : theme.inactiveColor},
      ]}>
      {title}
    </Text>
    {rightComponent}
  </TouchableOpacity>
);

export default withTheme(VerticalTab);

const styles = StyleSheet.create({
  container: {
    height: 40,
    borderRadius: 20,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
  },
  iconContainer: {
    borderRadius: 14,
    alignItems: 'center',
    justifyContent: 'center',
    height: 28,
    width: 28,
    marginLeft: 8,
    marginRight: 12,
  },
  text: {
    fontFamily: Fonts.regular,
    fontWeight: '400',
    fontSize: 16,
  },
});
