import React from 'react';
import {StyleProp, StyleSheet, View, ViewStyle} from 'react-native';
import Center from '../basics/Center';

interface Props {
  style?: StyleProp<ViewStyle>;
}

const BottomSheetHandleComponent: React.FC<Props> = ({style}) => {
  return (
    <Center style={[style, styles.container]}>
      <View style={styles.handle} />
    </Center>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 24,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
  },
  handle: {
    height: 4,
    width: 30,
    borderRadius: 2,
    backgroundColor: '#C4C4C4',
  },
});

export default BottomSheetHandleComponent;
