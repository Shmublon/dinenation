/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';

import './localization/i18n';

declare const global: {HermesInternal: null | {}};

import {StateProvider} from './store/context';
import MainNavigator from './navigation/MainNavigator';

const App: React.FC = () => {
  return (
    <StateProvider>
      <MainNavigator />
    </StateProvider>
  );
};

export default App;
