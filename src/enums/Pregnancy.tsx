export enum Pregnancy {
  NOT_PREGNANT,
  PREGNANT,
  NURSING,
}
