export enum DietFeature {
  NO_FEATURES,
  GLUTEN_FREE,
  LACTOSE_FREE,
}
