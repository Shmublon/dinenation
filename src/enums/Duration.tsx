export enum Duration {
  WEEK,
  MONTH,
  THREE_MONTH,
  SIX_MONTH,
}
