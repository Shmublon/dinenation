export enum DietType {
  NO_LIMITS,
  NO_MEAT,
  VEGETARIAN,
  VEGAN,
  NO_CARBONS,
}
